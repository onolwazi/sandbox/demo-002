FROM gitpod/workspace-full

USER gitpod

# Tools
RUN brew install exa && \
    brew install bat && \
    brew install httpie && \
    brew install hey
